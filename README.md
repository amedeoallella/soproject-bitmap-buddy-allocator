# [Progetto SO] Buddy Allocator – Implementazione tramite Bitmap

Il progetto consiste nell’implementazione di un buddy allocator usando una bitmap al posto degli alberi e delle liste collegate usate durante il corso.
Il buddy allocator divide ricorsivamente a metà la memoria disponibile e alloca il più piccolo spazio libero in grado di soddisfare l’iniziale richiesta di memoria da allocare. La memoria totale (e le sue partizioni) possono essere quindi rappresentate mediante un albero binario, in cui ciascun nodo rappresenta un blocco di memoria, la cui dimensione dipende dal livello dell’albero in cui si trova. Maggiore il livello, maggiore la profondità dell’albero e più piccoli I blocchi di memoria che si possono allocare.

***
La bitmap è stata realizzata mediante un array, dove ciascuna cella rappresenta un nodo della struttura ad albero: viene assegnato il valore 0 alle celle dell’array corrispondenti a blocchi di memoria liberi e 1 per quelli allocati.

Dopo aver inizializzato l’allocatore e la bitmap, si hanno a disposizione 2 funzioni per gestire la memoria: BuddyAllocator_malloc e BuddyAllocator_free.

## BuddyAllocator_malloc

La BuddyAllocator_malloc controlla inizialmente che la memoria richiesta sia minore di quella disponibile; successivamente fornisce alla funzione BuddyAllocator_getBuddy() il livello più grande nel quale è possibile allocare I blocchi della dimensione voluta. Vengono allocati 4 byte in più all’inizio della zona di memoria allocata nei quali viene scritto l’indice del blocco corrispondente nella bitmap/array.
Restituisce come risultato il puntatore alla zona di memoria allocata.

## BuddyAllocator_getBuddy

La BuddyAllocator_getBuddy fornisce l’indice del primo blocco di memoria libero nel livello passato in input, modifica il bit corrispondente nella bitmap e
aggiorna la bitmap/albero di conseguenza, mediante le funzioni update_ancestors e update_descendants.

## update_ancestors/update_descendants

Partendo dall’indice della bitmap corrispondente al blocco appena allocato (o liberato) le due funzioni modificano l’albero in modo ricorsivo, aggiornandone la struttura rispecchiando le modifiche ai bit.

## BuddyAllocator_free

Questa funzione recupera l’indice della bitmap corrispondente al blocco di memoria allocato e chiama la funzione BuddyAllocator_releaseBuddy.

## BuddyAllocator_releaseBuddy

La funzione imposta a 0 il bit nella bitmap nella posizione fornita dall’indice passato in input, il quale viene fornito dalla BuddyAllocator_free. Successivamente chiama le funzioni update_ancestors e update_descendants per aggiornare la bitmap.

***
### Le funzioni ausiliarie presenti nel programma sono in ordine:

- levelIdx: Dato in input un indice della bitmap ritorna il livello dell’albero corrispondente.
- BuddyIdx: Restituisce l’indice del buddy del nodo fornito in input
- parentIdx: Restituisce l’indice del genitore del nodo fornito in input
- firstIdx: Restituisce l’indice del primo nodo del livello fornito in input
- available_memory: Restituisce il quantitativo di memoria disponibile (in byte) al momento della chiamata.
- BitMap_print: Stampa su terminale la bitmap fornita in input, sotto forma di albero binario.

***
### Compilazione ed esecuzione

È disponibile un makefile per velocizzare il processo di compilazione:

- “make all” crea tutti I file, eseguibili ed ogetto, dei test.
- “make clean” rimuove I file compilati per lasciare solamente I file sorgente.
***
**Amedeo Allella**
