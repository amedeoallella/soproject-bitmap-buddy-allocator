#include <assert.h>
#include "bit_map.h"
#include <stdio.h>

//ritorna il numero di bytes necessari per immagazzinare bits valori di tipo boolean (0 o 1)
// returns the number of bytes to store bits booleans
int BitMap_getBytes(int bits){
  if((bits%8)==0) return bits/8;
  return bits/8 +1;
}


//inizializza una bitmap su un array esterno
// initializes a bitmap on an external array
void BitMap_init(BitMap* bit_map, int num_bits, uint8_t* buffer){
  bit_map->buffer=buffer;
  bit_map->num_bits=num_bits;
  bit_map->buffer_size=BitMap_getBytes(num_bits);
}

//setta il bit bit_num con il valore status (0 o 1)
// sets a the bit bit_num in the bitmap where status is either 0 or 1
void BitMap_setBit(BitMap* bit_map, int bit_num, int status){
  // get byte
  int byte_num=bit_num>>3;
  if(byte_num>=bit_map->buffer_size) return;
  //int bit_in_byte=byte_num&0x03;
  int bit_in_byte = 7-(bit_num%8);
  if (status) {
    bit_map->buffer[byte_num] |= (1<<bit_in_byte);
  } else {
    bit_map->buffer[byte_num] &= ~(1<<bit_in_byte);
  }
}

//controlla lo stato del bit bit_num (0 o 1)
// inspects the status of the bit bit_num
int BitMap_bit(const BitMap* bit_map, int bit_num){
  int byte_num=bit_num>>3; 
  assert(byte_num<bit_map->buffer_size);
  //int bit_in_byte=byte_num&0x03;
  int bit_in_byte = 7-(bit_num%8);
  return (bit_map->buffer[byte_num] & (1<<bit_in_byte))!=0;
}

void BitMap_print(BitMap* bit_map){

	BitMap* ptr = bit_map;
	int i = 0;
	printf("\n");
	while(i <72){
		printf(" ");
		i++;
	}
	printf("~~~~~~~~~~~stampa bitmap~~~~~~~~~~~\n");
	int num_bits = bit_map->num_bits;
	for(i = 1; i < num_bits ; i++){
		if(i == 1 || i == 2 || i==4 || i==8 || i==16 || i==32 || i==64 || i==128){
			printf("\n");
			for(int j=0; j<(90-i)+i/2;j++) printf(" ");
		}
		printf("%d",BitMap_bit(ptr,i));
	}
	i = 0;
	printf("\n");
	while(i <72){
		printf(" ");
		i++;
	}
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

}
