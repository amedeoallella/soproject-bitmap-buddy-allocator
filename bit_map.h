#pragma once
#include <stdint.h>

//array di bit | bit array
typedef struct  {
  uint8_t *buffer;  // buffer allocato esternamente | externally allocated buffer
  int buffer_size;
  int num_bits; 
} BitMap;

//ritorna il numero di bytes necessari per immagazzinare bits valori di tipo boolean (0 o 1)
//returns the number of bytes to store bits booleans
int BitMap_getBytes(int bits);

//inizializza la BitMap su un array esterno;
// initializes a bitmap on an external array
void BitMap_init(BitMap* bit_map, int num_bits, uint8_t* buffer);

//setta il bit bit_num con il valore status (0 o 1)
// sets a the bit bit_num in the bitmap where status is either 0 or 1
void BitMap_setBit(BitMap* bit_map, int bit_num, int status);

//controlla lo stato del bit bit_num (0 o 1)
// inspects the status of the bit bit_num
int BitMap_bit(const BitMap* bit_map, int bit_num);

//Stampa la bitmap su terminale
//Prints bitmap on terminal
void BitMap_print(BitMap* bit_map);
