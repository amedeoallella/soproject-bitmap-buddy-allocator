#include <stdio.h>
#include <assert.h>
#include <math.h>// log2 & floor functions
#include "bitmap_buddy.h"
#include <errno.h>
#include <stdlib.h>

//funzioni ausiliarie per la gestione dell'albero e dei suoi nodi
//auxiliary functions

int levelIdx(int idx){
	if(idx >= 1)return (int)floor(log2(idx));
	else{
		printf("\n[levelIdx] ERRORE: indice di partenza non valido\n");
		return -1;
	}
}

int buddyIdx(int idx){
	if(idx > 1){
		if(idx%2 == 0) return idx+1;
		else return idx-1;
		}
	printf("\n[buddyIdx] ERRORE: indice di partenza non valido\n");
	return -1;
}

int parentIdx(int idx){
	if(idx > 1)return floor(idx/2);
	printf("\n[parentIdx] ERRORE: indice nodo non valido\n");
	return -1;
}

int firstIdx(int level){
	if(level >=0)return 1<<level;
	printf("\n[firstIdx] ERRORE: livello non valido\n");
	return -1;
}

int areBuddies(int idx1, int idx2){
	if(idx1 > 1 && idx2 > 1){
		if(parentIdx(idx1) == parentIdx(idx2)) return 1;
		return 0;
		}
	printf("\n[areBuddies] ERRORE: uno dei due indici non è valido\n");
	return -1;
}

//implementazioni funzioni principali dell'allocatore
//main functions

int BuddyAllocator_calcSize(int num_levels){
	int num_bits = 1<<(num_levels+1); // num_levels + livello 0 della radice
	int size = (num_bits%8) ? (num_bits/8) : (num_bits/8 + 1); //calcolo multipli di byte
	if(size < 0){
		printf("\n[BuddyAllocator_calcSize] ERRORE: size < 0\n");
		return -1;
	}
	return size;
}

void BuddyAllocator_init(BuddyAllocator* alloc,int num_levels,uint8_t* buffer,int buffer_size,char* memory,int min_bucket_size){
	printf("\n[BuddyAllocator_init] inizializzazione allocatore...\n");
	alloc->num_levels = num_levels;
	alloc->memory = memory;
	alloc->min_bucket_size = min_bucket_size;
	printf("\n[BuddyAllocator_init] OK!\n");
	//controllo che il buffer sia grande abbastanza per contenere l'albero nella sua interezza
	assert(buffer_size >= BuddyAllocator_calcSize(num_levels));
	assert(num_levels<=MAX_LEVELS); //16
	
	printf("\n[BuddyAllocator_init] inizializzazione Bitmap...\n");
	int num_bits = 1<<(num_levels+1);//numero massimo di bit che posso allocare
	BitMap_init(&(alloc->bit_map),num_bits,buffer);
	
	
	for(int i = 0; i<num_bits;i++){
		BitMap_setBit(&(alloc->bit_map),i,0);
	}
	
	BitMap_print(&(alloc->bit_map));
	printf("\n[BuddyAllocator_init] OK!\n");
	printf("\nMemoria disponibile: %d Bytes\n",available_memory(alloc));
}


int BuddyAllocator_getBuddy(BuddyAllocator* alloc, int level){
	if(level < 0 || level > alloc->num_levels){printf("\n[BuddyAllocator_getBuddy] livello non valido\n");};
	
	int start_idx = firstIdx(level);
	int end_idx = firstIdx(level+1)-1;
	
	//faccio una ricerca lineare sul livello per trovare l'indice del buddy
	int i;
	for(i = start_idx;i<=end_idx; i++){
		if((BitMap_bit(&(alloc->bit_map),i)) == 0){//blocco di memoria libero
			BitMap_setBit(&(alloc->bit_map),i,1);
			update_ancestors(&(alloc->bit_map),i);
			//printf("\n[update_ancestors] Operazione completata con successo\n");
			update_descendants(&(alloc->bit_map),i);
			//printf("\n[update_descendants] Operazione completata con successo\n");
			return i;
			}
		}
	printf("\n[getBuddy] ERRORE: non è stato trovato un buddy da allocare al livello richiesto\n");
	return -1;
}

void update_ancestors(BitMap *map, int curr_idx){
	if(curr_idx > 1){//1 is start of tree, no parent node
		int buddy_idx = buddyIdx(curr_idx);
		int parent_idx = parentIdx(curr_idx);
		
		if((BitMap_bit(map,curr_idx) == 1) || (BitMap_bit(map,buddy_idx) == 1)){
			//parent 0, curr and buddy both 1, update parent to 1
			BitMap_setBit(map,parent_idx,1);
			//printf("[update_ancestors] aggiornato %d, genitore di %d, con il valore %d\n",parent_idx,curr_idx,1);
		}
		else if((BitMap_bit(map,curr_idx) == 0) && (BitMap_bit(map,buddy_idx) == 0)){
			BitMap_setBit(map,parent_idx,0);
			//printf("[update_ancestors] aggiornato %d, genitore di %d, con il valore %d\n",parent_idx,curr_idx,0);
		}
		update_ancestors(map,parent_idx);
	}
}

void update_descendants(BitMap *map,int curr_idx){
	if(levelIdx(curr_idx) < levelIdx(map->num_bits)){
		int left_son_idx = 2*curr_idx;
		int right_son_idx = left_son_idx+1;
		
		if((BitMap_bit(map,curr_idx) == 1) ){
			BitMap_setBit(map,left_son_idx,1);
			//printf("[Update_descendants] aggiornato %d, figlio sinistro di %d, con il valore %d\n",left_son_idx,curr_idx,1);
			BitMap_setBit(map,right_son_idx,1);
			//printf("[Update_descendants] aggiornato %d, figlio destro di %d, con il valore %d\n",right_son_idx,curr_idx,1);
		}
		else if((BitMap_bit(map,curr_idx) == 0) ){
			BitMap_setBit(map,left_son_idx,0);
			//printf("[Update_descendants] aggiornato %d, figlio sinistro di %d, con il valore %d\n",left_son_idx,curr_idx,0);
			BitMap_setBit(map,right_son_idx,0);
			//printf("[Update_descendants] aggiornato %d, figlio destro di %d, con il valore %d\n",right_son_idx,curr_idx,0);
		}
		update_descendants(map,left_son_idx);
		update_descendants(map,right_son_idx);
	}
}

			
void *BuddyAllocator_malloc(BuddyAllocator* alloc,int size){
	printf("\nRichiesti: %d byte\n", size);
	if(size > MEM_LIMIT){
		printf("\n[BuddyAllocator_malloc] ERRORE: La memoria richiesta oltrepassa il totale disponibile\n");
		return 0;
	}
	
	int memory_size = (1<<alloc->num_levels)*alloc->min_bucket_size;
	//printf("memory_size = %d\n",memory_size);
	int level = floor(log2(memory_size/(size+4)));
	//printf("level = %d\n",level);
	if (level>alloc->num_levels)level=alloc->num_levels;
	//printf("level = %d\n",level);
	
    printf("\nAllocazione al livello %d, dimensione blocchi: %d\n",level,MEMORY_SIZE/(1<<level));
    //indice blocco libero nella bitmap
    int free_buddy = BuddyAllocator_getBuddy(alloc,level); //indice del buddy per la dimensione richiesta
    if(free_buddy == -1){
    	printf("\n[BuddyAllocator_malloc] ERRORE: buddy invalido\n");
    	return 0;
    }
    
    int offset = free_buddy%(1<<level); //offset del blocco di memoria nel livello
    int buddy_size = memory_size>>level; // dimensione del blocco di memoria
    
    //creo un puntatore alla zona di memoria da allocare
    int *free_buddy_pointer = (int*)(alloc->memory+(offset*buddy_size));
    
    //scrivo dentro il puntatore l'indice del buddy, int = 4 byte
    *free_buddy_pointer = free_buddy;
    //printf("%d\n",*free_buddy_pointer);
    
    printf("\nLa memoria allocata comincia all'indirizzo: %p\n",free_buddy_pointer+4);
    printf("\nMemoria disponibile dopo l'allocazione: %d Bytes\n",available_memory(alloc));		
    
    BitMap_print(&(alloc->bit_map));
    
    return free_buddy_pointer+4; // la memoria comincia dopo i 4 byte di bookkeeping
}

void BuddyAllocator_free(BuddyAllocator* alloc, void* mem){
	//controllo se la memoria è effettivamente allocata
	if(mem == 0 || mem == NULL){
		printf("\n[BuddyAllocator_free] ERRORE: memoria non allocata, impossibile eseguire free\n");
		return ;
	}
	
	int *ptr = mem;
	ptr = ptr-4; //la memoria comprende i 4 byte iniziali di bookkeeping
	//printf("\nnode: %d\n", *ptr); //controllo che il valore del buddy sia corretto
	
	BuddyAllocator_releaseBuddy(alloc,*ptr);
	printf("\n[BuddyAllocator_free] memoria liberata correttamente\n");
	printf("\nMemoria disponibile dopo la deallocazione: %d Bytes\n",available_memory(alloc));
	BitMap_print(&(alloc->bit_map));
}


void BuddyAllocator_releaseBuddy(BuddyAllocator *alloc, int buddy){

	if(buddy < 0){
		printf("\n[BuddyAllocator_releaseBuddy] ERRORE: buddy invalido\n");
		return ;
	}
	
	BitMap *map = &(alloc->bit_map);
	int num_bits = map->num_bits;
	assert(buddy <= num_bits);
	
	if(BitMap_bit(&(alloc->bit_map),buddy) == 0){
		printf("\n[BuddyAllocator_releaseBuddy] il buddy non è occupato\n");
		return ;
	}
	
	BitMap_setBit(&(alloc->bit_map),buddy,0);
	printf("\n[BuddyAllocator_releaseBuddy] buddy rilasciato\n");
	
	update_ancestors(&(alloc->bit_map),buddy);
	update_descendants(&(alloc->bit_map),buddy);
}

int available_memory(BuddyAllocator *alloc){
	int i=firstIdx(alloc->num_levels);
	int j = 2*i;
	int res = 0;
	for(; i<j; i++){
		if(BitMap_bit(&(alloc->bit_map),i) == 0){
			res+=MIN_BUCKET_SIZE;
		}
	}
	return res;
}
