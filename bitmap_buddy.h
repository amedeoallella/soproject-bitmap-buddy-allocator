#pragma once
#include "bit_map.h"
#include <stdint.h>

#define MAX_LEVELS 8
#define MEM_LIMIT 1016
#define BUFFER_SIZE 10240//10KB
#define BUDDY_LEVELS 7
#define MEMORY_SIZE 1024 //cioè 1KB = 1024 B
#define MIN_BUCKET_SIZE MEMORY_SIZE>>BUDDY_LEVELS // 1024 / 128 = 8B 

//il buddy allocator ha la stessa struttura della versione con le liste
//uso bitmap invece delle liste
typedef struct  {
  BitMap bit_map;
  int num_levels;
  char* memory; // the memory area to be managed
  int min_bucket_size; // the minimum page of RAM that can be returned
} BuddyAllocator;

// calcola la dimensione in bytes necessaria per il buffer dell'allocatore
// computes the size in bytes for the buffer of the allocator
int BuddyAllocator_calcSize(int num_levels);


//inizializza il buddy allocator e controlla che il buffer sia grande abbastanza
// initializes the buddy allocator, and checks that the buffer is large enough
void BuddyAllocator_init(BuddyAllocator* alloc,
                         int num_levels,
                         uint8_t* buffer,
                         int buffer_size,
                         char* memory,
                         int min_bucket_size);

// ritorna (alloca) un buddy ad un dato livello
// returns (allocates) a buddy at a given level.
// side effect on the internal structures
// 0 id no memory available
int BuddyAllocator_getBuddy(BuddyAllocator* alloc, int level);


//rilascia un buddy allocato
// releases an allocated buddy, performing the necessary joins
// side effect on the internal structures
void BuddyAllocator_releaseBuddy(BuddyAllocator* alloc, int buddy);

//alloca memoria
//allocates memory
void* BuddyAllocator_malloc(BuddyAllocator* alloc, int size);

//rilascia la memoria allocata
//releases allocated memory
void BuddyAllocator_free(BuddyAllocator* alloc, void* mem);

//aggiorna nodi antenati
void update_ancestors(BitMap *map, int curr_idx);

//aggiorna nodi successori
void update_descendants(BitMap *map, int curr_idx);

//ritorna la quantità di memoria disponibile (in bytes)
int available_memory(BuddyAllocator *alloc);
