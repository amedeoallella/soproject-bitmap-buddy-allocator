#include <stdio.h>
#include <assert.h>
#include "bitmap_buddy.h"

uint8_t buffer[BUFFER_SIZE];
char memory[MEMORY_SIZE];

/*Allocazione di 3 blocchi di memoria con dimensione incrementale: 50 byte, 200 byte e 300 byte.*/
/*Deallocazione dei blocchi nell'ordine in cui sono stati allocati*/
int main(){
	BuddyAllocator alloc;
	BuddyAllocator_init(&alloc,BUDDY_LEVELS,buffer,BUFFER_SIZE,memory,MIN_BUCKET_SIZE);
 	
 	void* b1=BuddyAllocator_malloc(&alloc, 50);
 	void* b2=BuddyAllocator_malloc(&alloc, 200);
 	void* b3=BuddyAllocator_malloc(&alloc, 500);
 	
 	BuddyAllocator_free(&alloc, b1);
 	BuddyAllocator_free(&alloc, b2);
 	BuddyAllocator_free(&alloc, b3);
 	
 	return 1;
}
