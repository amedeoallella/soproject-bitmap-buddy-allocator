#include <stdio.h>
#include <assert.h>
#include "bitmap_buddy.h"

uint8_t buffer[BUFFER_SIZE];
char memory[MEMORY_SIZE];

/*Allocazione di 4 blocchi di dimensione non incrementale, la deallocazione non segue lo stesso ordine*/
int main(){
	BuddyAllocator alloc;
	BuddyAllocator_init(&alloc,BUDDY_LEVELS,buffer,BUFFER_SIZE,memory,MIN_BUCKET_SIZE);
	
	void* b1=BuddyAllocator_malloc(&alloc, 129);
 	void* b2=BuddyAllocator_malloc(&alloc, 356);
 	void* b3=BuddyAllocator_malloc(&alloc, 89);
 	void* b4=BuddyAllocator_malloc(&alloc, 25);
 	
 	BuddyAllocator_free(&alloc, b3);
 	BuddyAllocator_free(&alloc, b1);
 	BuddyAllocator_free(&alloc, b2);
 	BuddyAllocator_free(&alloc, b4);
 	
 	return 1;
}
