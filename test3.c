#include <stdio.h>
#include <assert.h>
#include "bitmap_buddy.h"

uint8_t buffer[BUFFER_SIZE];
char memory[MEMORY_SIZE];

/*Allocazione di un blocco di memoria di dimensione maggiore della memoria totale, segue un'allocazione normale.*/
/*Allocazione di un blocco con dimensione minore di quella della memoria totale ma maggiore di quella disponibile.*/
/*Le deallocazioni dei blocchi non allocati falliscono come dovuto*/
int main(){
	BuddyAllocator alloc;
	BuddyAllocator_init(&alloc,BUDDY_LEVELS,buffer,BUFFER_SIZE,memory,MIN_BUCKET_SIZE);
 	
 	void* b1=BuddyAllocator_malloc(&alloc, 1110); //fallisce, maggiore della memoria totale (1024 B)
 	void* b2=BuddyAllocator_malloc(&alloc, 111);
 	void* b3=BuddyAllocator_malloc(&alloc, 973); // fallisce, maggiore della memoria disponibile al momento dell'allocazione
 	void* b4=BuddyAllocator_malloc(&alloc, 52);
 	
 	BuddyAllocator_free(&alloc, b1); //fallisce poichè non allocato
 	BuddyAllocator_free(&alloc, b2);
 	BuddyAllocator_free(&alloc, b3); //fallisce poichè non allocato
 	BuddyAllocator_free(&alloc, b4);
 	
 	return 1;
}
