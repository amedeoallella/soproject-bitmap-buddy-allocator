#include <stdio.h>
#include <assert.h>
#include "bitmap_buddy.h"

uint8_t buffer[BUFFER_SIZE];
char memory[MEMORY_SIZE];

/*Allocazione di tutti i blocchi in un livello, un ulteriore richiesta allo stesso livello fallisce*/
int main(){
	BuddyAllocator alloc;
	BuddyAllocator_init(&alloc,BUDDY_LEVELS,buffer,BUFFER_SIZE,memory,MIN_BUCKET_SIZE);
 	
 	void* b1=BuddyAllocator_malloc(&alloc, 200);
 	void* b2=BuddyAllocator_malloc(&alloc, 200);
 	void* b3=BuddyAllocator_malloc(&alloc, 200);
 	void* b4=BuddyAllocator_malloc(&alloc, 200);
 	
 	void* b5=BuddyAllocator_malloc(&alloc, 200); //fallisce perchè il livello è occupato nella sua interezza
 	
 	BuddyAllocator_free(&alloc, b1);
 	BuddyAllocator_free(&alloc, b2);
 	BuddyAllocator_free(&alloc, b3);
 	BuddyAllocator_free(&alloc, b4);

 	BuddyAllocator_free(&alloc, b5); //fallisce perchè quel blocco non è mai stato allocato
 	
 	return 1;
}
