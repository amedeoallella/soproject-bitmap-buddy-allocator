#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "bitmap_buddy.h"

uint8_t buffer[BUFFER_SIZE];
char memory[MEMORY_SIZE];

/*Allocazione di 4 blocchi di memoria di dimensione casuale, deallocazione nello stesso ordine*/
int main(){
	BuddyAllocator alloc;
	BuddyAllocator_init(&alloc,BUDDY_LEVELS,buffer,BUFFER_SIZE,memory,MIN_BUCKET_SIZE);
 	
 	int block_size;
 	srand((unsigned int)time(NULL));
 	
 	//la dimensione del blocco è un numero casuale tra 0 e 500
	block_size = rand() % 500;
	void* b1 = BuddyAllocator_malloc(&alloc,block_size);
	block_size = rand() % 500;
	void* b2 = BuddyAllocator_malloc(&alloc,block_size);
	block_size = rand() % 500;
	void* b3 = BuddyAllocator_malloc(&alloc,block_size);
	block_size = rand() % 500;
	void* b4 = BuddyAllocator_malloc(&alloc,block_size);
 	
 	BuddyAllocator_free(&alloc, b1);
 	BuddyAllocator_free(&alloc, b2);
 	BuddyAllocator_free(&alloc, b3);
 	BuddyAllocator_free(&alloc, b4);
 	
 	return 1;
}
