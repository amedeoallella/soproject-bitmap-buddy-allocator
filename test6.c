#include <stdio.h>
#include <assert.h>
#include "bitmap_buddy.h"

uint8_t buffer[BUFFER_SIZE];
char memory[MEMORY_SIZE];

/*Allocazione e deallocazione consecutive della stessa zona di memoria, che viene correttamente riutilizzata dopo essere stata liberata*/
int main(){
	BuddyAllocator alloc;
	BuddyAllocator_init(&alloc,BUDDY_LEVELS,buffer,BUFFER_SIZE,memory,MIN_BUCKET_SIZE);
 	
 	void* b1=BuddyAllocator_malloc(&alloc, 200); 	
 	BuddyAllocator_free(&alloc, b1);

 	b1=BuddyAllocator_malloc(&alloc, 200); 	
 	BuddyAllocator_free(&alloc, b1);

 	b1=BuddyAllocator_malloc(&alloc, 200); 	
 	BuddyAllocator_free(&alloc, b1);

 	b1=BuddyAllocator_malloc(&alloc, 200); 	
 	BuddyAllocator_free(&alloc, b1);
	
 	return 1;
}
