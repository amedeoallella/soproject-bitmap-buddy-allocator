#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include "bitmap_buddy.h"

uint8_t buffer[BUFFER_SIZE];
char memory[MEMORY_SIZE];

/*Allocazione di 3 blocchi di dimensione casuale al livello 2, seguita da un'allocazione al livello 1 (fallisce perchè livello 1 pieno) ed un'ultima allocazione al livello 2.*/
int main(){
	BuddyAllocator alloc;
	BuddyAllocator_init(&alloc,BUDDY_LEVELS,buffer,BUFFER_SIZE,memory,MIN_BUCKET_SIZE);
 	
 	int block_size;
 	srand((unsigned int)time(NULL));
 	
 	//allocazione di 3 blocchi di dimensione casuale al livello 2
 	block_size = (rand() % (128))+124;
	void* b1 = BuddyAllocator_malloc(&alloc,block_size);
	block_size = (rand() % (128))+124;
	void* b2 = BuddyAllocator_malloc(&alloc,block_size);
	block_size = (rand() % (128))+124;
	void* b3 = BuddyAllocator_malloc(&alloc,block_size);
	
	block_size = (rand() % (256))+252;//allocazione di un blocco di dimensione casuale al livello 1
	void* b4 = BuddyAllocator_malloc(&alloc,block_size);//fallisce
	
	block_size = (rand() % (128))+124;
	void* b5 = BuddyAllocator_malloc(&alloc,block_size);//allocazione di un ultimo blocco di dimensione casuale al livello 2
	
	BuddyAllocator_free(&alloc, b1);
 	BuddyAllocator_free(&alloc, b2);
 	BuddyAllocator_free(&alloc, b3);
 	
 	BuddyAllocator_free(&alloc, b4);//fallisce
 	
 	BuddyAllocator_free(&alloc, b5);
 	return 1;
}
